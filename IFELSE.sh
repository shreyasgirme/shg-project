#! bin/bash
Action=$1

if [ "$Action" = "start" ];then
echo "starting the service"

elif [ "$Action" = "stop" ];then
echo "stopping the service"

elif [ "$Action" = "restart" ];then
echo "restarting the service."

else echo "Availabile options are start /stop/Restart"
exit 1
fi
