#!/bin/bash
ID=$(id -u)
if [ $ID -ne 0 ]; then
echo "you should be running the script as root user"
exit 1
fi

stat() {
if [ $1 -eq 0 ] ; then
echo -e "\e[32m Success \e[0m"
else
echo -e "\e[31m failure \e[0m"
fi
}

LOG=/tmp/stack.log
FUSER=student
VERSION="8.5.76"
APACHE_URL="https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.76/bin/apache-tomcat-$VERSION.tar.gz"
WAR_URL="https://devops-cloudcareers.s3.ap-south-1.amazonaws.com/student.war"
JDBC_URL="https://devops-cloudcareers.s3.ap-south-1.amazonaws.com/mysql-connector.jar"

#Setting up Web server

echo -n "installing apache Webserver"
yum install httpd -y &> $LOG
stat $?

echo -n "creating proxy config :"
echo 'ProxyPass "/student" "http://localhost:8080/student"
ProxyPassReverse "/student"  "http://localhost:8080/student" ' >/etc/httpd/conf.d/proxy.conf
stat $?

echo -n "set up default index file"
curl -s https://learndevopswithaws.s3.ap-south-1.amazonaws.com/index.html >/var/www/html/index.html
stat $?

echo -n "Starting the web service"
systemctl start httpd &> $LOG
systemctl enable httpd &> $LOG
stat $?

# Setting up App Server

echo -n "installing Java"
yum install java -y &> $LOG
stat $?

echo -n "creating the $FUSER  functional user : "
id $FUSER &> $LOG
if [ $? -eq 0 ]; then 
      echo -e "\e[33m Skipping, $FUSER already exists \e[0m"
else 
     useradd $FUSER &> $LOG
     stat $?
fi

echo -n "Downloading the Tomact"
cd /home/$FUSER/
wget $APACHE_URL &> $LOG
tar -xf apache-tomcat-$VERSION.tar.gz &> $LOG
stat $?     

echo -n "downloading the $FUSER war file"
wget $WAR_URL -P apache-tomcat-$VERSION/webapps  &> $LOG
chown -R $FUSER:$FUSER  apache-tomcat-$VERSION
stat $?

echo -n "downloading JDBC : "
wget $JDBC_URL -P apache-tomcat-$VERSION/lib  &> $LOG
stat $?

echo "starting the tomcat"
sh apache-tomcat-$VERSION/bin/startup.sh &> $LOG
stat $?

